//
//  UserManager.swift
//  MyTestCoreData-01
//
//  Created by Vlad on 11/18/20.
//

import Foundation
import CoreData

final class UserManager {
    func createNewUser(success: ([User]) -> ()) {
        let book = Book(context: CoreDataManager.context)
        
        book.titleBook = "Book \(Int.random(in: 0...Int.max))"
        
        let newUser = User(context: CoreDataManager.context)
        
        newUser.name = "User \(Int.random(in: 0...Int.max))"
        newUser.book = book
        
        CoreDataManager.saveContext()
        
        getUserList(success: success)
    }
    
    func getUserList(success: ([User]) -> ()) {
        let descript = NSSortDescriptor(key: #keyPath(User.name), ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        
        fetchRequest.sortDescriptors = [descript]
        
        do {
            let res = try CoreDataManager.context.fetch(fetchRequest)
            
            if !res.isEmpty {
                success(res)
            }
            
        } catch let err as NSError {
            print("\(err.userInfo)")
        }
    }
}
