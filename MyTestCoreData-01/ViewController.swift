//
//  ViewController.swift
//  MyTestCoreData-01
//
//  Created by Vlad on 11/18/20.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let userManager = UserManager()
    let cellId = "cell"
    var userList: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        userManager.getUserList { res in
            userList = res
            tableView.reloadData()
        }
    }

    @IBAction func addAction(_ sender: Any) {
        userManager.createNewUser { res in
            userList = res
            tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: cellId,
            for: indexPath
        )
        
        cell.textLabel?.text = userList[indexPath.row].name
        
        if let titleBook = userList[indexPath.row].book?.titleBook {
            cell.detailTextLabel?.text = titleBook
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        let userForRemove = userList[indexPath.row]
        
        CoreDataManager.context.delete(userForRemove)
        CoreDataManager.saveContext()
        
        userManager.getUserList { userList in
            self.userList = userList
        }

        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
}
