//
//  Book+CoreDataProperties.swift
//  MyTestCoreData-01
//
//  Created by Vlad on 11/18/20.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var titleBook: String?
    @NSManaged public var owner: User?

}

extension Book : Identifiable {

}
